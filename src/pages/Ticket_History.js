import React from 'react'
import './Ticket_History.css';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';

function Ticket_History() {
    return (
        <div>
            <div class="container_right">
                <div class="search_container">
                    <div className="search_by" >Search By</div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                        <label class="form-check-label" for="flexRadioDefault2">Order No</label>
                        <div>
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                            <label class="form-check-label" for="flexRadioDefault2">Escalation No</label>
                        </div>
                    </div>

                    <div className="search_button">
                        <form className="form_group">
                            <input type="text" placeholder="Search.." className="search" />
                            <button type="button" class="btn btn-dark btn-lg" >Search</button>
                            {/* <a href="#" class="btn btn-dark btn-lg active" role="button" aria-pressed="true">Search</a> */}

                        </form>
                    </div>
                </div>
            </div>
            <div class="row1">
                <div className="contaner_card">
                    <div className="container2">
                        <div className="card_top">
                            <div className="card_top_left">
                                <div className="card_top_left_item1">Order 27761331</div>
                                <div className="card_top_left_item2"> Ticket 1655249</div>
                            </div>
                            <div className="card_top_right">
                                <button class="button button1">Resolve</button>
                                <div className="card_top_right_item2"> <ThumbUpIcon /> </div>
                            </div>
                            <div className="drawline3"></div>
                        </div>
                        <div className="bottom_container">
                            <div className="first_row_container">
                                <div>
                                    <div className="td_head" ><div className="font_upper_css">Ticket Date & Time Stamp</div></div>
                                    <div className="td_bottom"><div className="font_bottom_css">09/10/2011 ; 2:00PM</div> </div>
                                </div>
                                <div>
                                    <div className="td_head"><div className="font_upper_css">Order Amount</div></div>
                                    <div className="td_bottom"><div className="font_bottom_css">₹150</div> </div>
                                </div>
                                <div>
                                    <div className="td_head"><div className="font_upper_css">Ticket Platform</div></div>
                                    <div className="td_bottom"><div className="font_bottom_css">Chat </div></div>
                                </div>
                            </div>

                            <div className="second_row_container">
                                <div>
                                    <div className="td_head">Ordert Platform</div>
                                    <div className="td_bottom">Zomato </div>
                                </div>
                                <div>
                                    <div className="td_head">Agent Name</div>
                                    <div className="td_bottom">Ajay Sharma</div>
                                </div>
                                <div>
                                    <div className="td_head">Food & Delivery Rating</div>
                                    <div className="td_bottom">4.5 ; 4.2</div>
                                </div>
                            </div>


                            <div className="third_row_container">
                                <div className="td_head">Ticket Child Bucket</div>
                                <div className="td_bottom">Other urgent issues </div>
                            </div>
                            <div className="fourth_row_conatiner">
                                <div className="td_head">Ticket Child Bucket</div>
                                <div className="td_bottom">Additional information regarding order </div>
                            </div>
                        </div>

                    </div>


                    <div className="container2">

                        <div>
                            <div className="card_top">
                                <div className="card_top_left">
                                    <div className="card_top_left_item1">Order 27761331</div>
                                    <div className="card_top_left_item2"> Ticket 1655249</div>
                                </div>
                                <div className="card_top_right">
                                    <div><button class="button2">Resolve</button></div>
                                    <div className="card_top_right_item2"> <ThumbDownIcon /> </div>
                                </div>
                                <div className="drawline3"></div>
                            </div>
                            <div className="bottom_container">
                                <div className="first_row_container">
                                    <div>
                                        <div className="td_head" ><div className="font_upper_css">Ticket Date & Time Stamp</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">09/10/2011 ; 2:00PM</div> </div>
                                    </div>
                                    <div>
                                        <div className="td_head"><div className="font_upper_css">Order Amount</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">₹150</div> </div>
                                    </div>
                                    <div>
                                        <div className="td_head"><div className="font_upper_css">Ticket Platform</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">Chat </div></div>
                                    </div>
                                </div>

                                <div className="second_row_container">
                                    <div>
                                        <div className="td_head">Ordert Platform</div>
                                        <div className="td_bottom">Zomato </div>
                                    </div>
                                    <div>
                                        <div className="td_head">Agent Name</div>
                                        <div className="td_bottom">Ajay Sharma</div>
                                    </div>
                                    <div>
                                        <div className="td_head">Food & Delivery Rating</div>
                                        <div className="td_bottom">4.5 ; 4.2</div>
                                    </div>
                                </div>


                                <div className="third_row_container">
                                    <div className="td_head">Ticket Child Bucket</div>
                                    <div className="td_bottom">Other urgent issues </div>
                                </div>
                                <div className="fourth_row_conatiner">
                                    <div className="td_head">Ticket Child Bucket</div>
                                    <div className="td_bottom">Additional information regarding order </div>
                                </div>
                            </div>

                        </div>



                    </div>
                    <div className="container2">

                        <div>
                            <div className="card_top">
                                <div className="card_top_left">
                                    <div className="card_top_left_item1">Order 27761331</div>
                                    <div className="card_top_left_item2"> Ticket 1655249</div>
                                </div>
                                <div className="card_top_right">
                                    <div>                    <button class="button button1">Resolve</button>
                                    </div>
                                    <div className="card_top_right_item2"> <ThumbUpIcon /> </div>
                                </div>
                                <div className="drawline3"></div>
                            </div>
                            <div className="bottom_container">
                                <div className="first_row_container">
                                    <div>
                                        <div className="td_head" ><div className="font_upper_css">Ticket Date & Time Stamp</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">09/10/2011 ; 2:00PM</div> </div>
                                    </div>
                                    <div>
                                        <div className="td_head"><div className="font_upper_css">Order Amount</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">₹150</div> </div>
                                    </div>
                                    <div>
                                        <div className="td_head"><div className="font_upper_css">Ticket Platform</div></div>
                                        <div className="td_bottom"><div className="font_bottom_css">Chat </div></div>
                                    </div>
                                </div>

                                <div className="second_row_container">
                                    <div>
                                        <div className="td_head">Ordert Platform</div>
                                        <div className="td_bottom">Zomato </div>
                                    </div>
                                    <div>
                                        <div className="td_head">Agent Name</div>
                                        <div className="td_bottom">Ajay Sharma</div>
                                    </div>
                                    <div>
                                        <div className="td_head">Food & Delivery Rating</div>
                                        <div className="td_bottom">4.5 ; 4.2</div>
                                    </div>
                                </div>


                                <div className="third_row_container">
                                    <div className="td_head">Ticket Child Bucket</div>
                                    <div className="td_bottom">Other urgent issues </div>
                                </div>
                                <div className="fourth_row_conatiner">
                                    <div className="td_head">Ticket Child Bucket</div>
                                    <div className="td_bottom">Additional information regarding order </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Ticket_History;
