import React from 'react';
import './App.css';
import  '../node_modules/bootstrap/dist/css/bootstrap.css';
import Customer_Details_page from './pages/Customer_Details_page';
import Ticket_History from './pages/Ticket_History';
import Credit_History from './pages/Credit_History';
import Order_History from './pages/Order_History';

import Navbar from './layout/Navbar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Notfound from "./pages/Notfound";

function App() {
  return (
    <>
      <Router>
        <div className="App">
                <body>
                      <div className="navbar_container"><Navbar /></div>
                  <div id="main">
                      <div>
                              <Switch>
                                <Route exact path="/" component={Customer_Details_page} />
                                <Route exact path="/Ticket_History" component={Ticket_History} />
                                <Route exact path="/Credit_History" component={Credit_History} />
                                <Route exact path="/Order_History" component={Order_History} />
                                <Route component={Notfound} />
                              </Switch>
                      </div>
                      <nav className="side_navbar">Side Navbar</nav>
                  </div>
                </body>         
          </div>
      </Router>
       {/* <Navbar /> */}
                      {/* <Switch>
                        <Route exact path="/" component={Customer_Details_page} />
                        <Route exact path="/Ticket_History" component={Ticket_History} />
                        <Route exact path="/Credit_History" component={Credit_History} />
                        <Route exact path="/Order_History" component={Order_History} />
                        <Route component={Notfound} />
                      </Switch> */}
    </>
  );
}

export default App;
