import React from 'react';
import { Link, NavLink } from "react-router-dom";

const Navbar = () => {
return (
<nav className="navbar navbar-expand-lg navbar-dark bg-primary colour">
  <div className="container" >
  <Link className="navbar-brand" href="/"></Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" >
    <ul className="navbar-nav mr-auto">
      <li className="nav-item navbar_item">
        <NavLink className="nav-link navbar_item" exact to="/">Customer Details </NavLink>
      </li>
      <li className="nav-item navbar_item">
        <NavLink className="nav-link navbar_item" exact to="/Ticket_History">Ticket History</NavLink>
      </li>
      <li className="nav-item navbar_item">
        <NavLink className="nav-link navbar_item" exact to="/Credit_History">Credit History</NavLink>
      </li>
      <li className="nav-item navbar_item">
        <NavLink className="nav-link navbar_item" exact to="/Order_History">Order History</NavLink>
      </li>
    </ul>
  </div>
  </div>
</nav>
);
};


export default Navbar;